        <?php
        if ( !defined( 'ABSPATH' ) ) { exit; }
        if ( ! isset( $content_width ) ) $content_width = 1280;

        if ( ! defined( 'DYNOSHOP_VERSION' ) ) {
            define( 'DYNOSHOP_VERSION', '2.3' );
        }

        // Required Functions Start
        require __DIR__ . '/vendor/autoload.php';

        require get_template_directory() . '/inc/enqueue.php';   // Script Enqueue Functions
        require get_template_directory() . '/inc/theme-setup.php';  // All Theme Setup Functions
        require get_template_directory() . '/inc/admin.php';  // Admin Panel by CSF
        require get_template_directory() . '/inc/demo-import.php';


        // Auto Updater Function

        $WPDynoshopUpdater = Puc_v4_Factory::buildUpdateChecker(
            'https://gitlab.com/anonnaabir/wp-dynoshop',
            __FILE__,
            'wp-dynoshop'
        );



        function wp_dynoshop_register_elementor_locations( $elementor_theme_manager ) {
            $elementor_theme_manager->register_all_core_location();
        };
        add_action( 'elementor/theme/register_locations', 'wp_dynoshop_register_elementor_locations' );


