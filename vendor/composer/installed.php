<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '911f51fa48dedbce451488b96b26b846fc1051b9',
        'name' => 'anonnaabir/wp-dynoshop',
        'dev' => true,
    ),
    'versions' => array(
        'anonnaabir/wp-dynoshop' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '911f51fa48dedbce451488b96b26b846fc1051b9',
            'dev_requirement' => false,
        ),
        'yahnis-elsts/plugin-update-checker' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../yahnis-elsts/plugin-update-checker',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => '8d78365380aa04e9e2601cdf21b8e3c45127b44b',
            'dev_requirement' => false,
        ),
    ),
);
