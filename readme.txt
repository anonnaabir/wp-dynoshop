=== Dynoshop ===

Requires at least: 4.5
Tested up to: 5.8.1
Requires PHP: 7.0
Stable tag: 2.0
License: GNU General Public License v2 or later
License URI: LICENSE

An all-in-one & drag and drop eCommerce Theme For WordPress.

== Description ==

An all-in-one & drag and drop eCommerce Theme For WordPress. Compatible with WooCommerce,Easy Digital Download and Elementor.



== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.



== Changelog ==

= 1.0 - November 01, 2021 =
* Initial release

= 1.1 - December 26, 2021 =
* Rebuilt From Scratch
* New Dedicated Option Panel
* New Required Plugin Installer
* Added White Label Feature
* Compatible and Tested with WordPress 5.8.1
* Performance Update

= 2.0 - May 28, 2022 =
* Added Divi Builder Support (Divi Child Theme)
* Added Advanced Product Filtering / Shorting (Elementor)
* Added Product Image Gallery (Elementor)
* Added 'Gaming Hardware Shop' Demo (Elementor)
* Added 'Kids Toy Shop' Demo (Elementor)
* Added 'Tools & Home Improvement' Demo (Elementor)
* Added 'Automobile Shop' Demo (Elementor)
* Added 'Book Shop' Demo (Divi)
* Added 'Dog Food' Demo (Divi)
* Compatible and Tested with WordPress 6.0
* Compatible and Tested with WooCommerce 6.5.1
* Performance Update
* Option Panel Framework Update
* Additional Bug Fixed


= 2.3 - Jul 01, 2022 =

* Automatic Update Feature Added
* Compatible and Tested with WooCommerce 6.6.1
* Performance Update
* Additional Bug Fixed