<?php

			// Demo Import Function Start

			function wp_dynoshop_demo_import() {
				return array(
					array(
						'import_file_name'           => 'Gaming Hardware Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/gaminghardwareshop.WordPress.2022-05-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2022/04/dynoshop_gaming_hardware-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/gaming-hardware-shop/',
					),
					
					array(
						'import_file_name'           => 'Medical Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/medicalshop.WordPress.2022-05-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2022/04/dynoshop_medicalshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/medical-shop/',
					),
					
					array(
						'import_file_name'           => 'Kids Toy Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/kidstoyshop.WordPress.2022-05-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2022/04/dynoshop_kids_toy_shop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/kids-toy-shop/',
					),


					array(
						'import_file_name'           => 'Tools Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/toolsamphomeimprovement.WordPress.2022-05-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2022/04/dynoshop_tools_shop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/tools-and-home-improvement/',
					),


					array(
						'import_file_name'           => 'Automobile Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/automobileshop.WordPress.2022-05-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2022/04/dynoshop_automobileshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/automobile-shop/',
					),
					
					array(
						'import_file_name'           => 'Camera Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/camerashop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_camerashop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/camera-shop/',
					),

					array(
						'import_file_name'           => 'Furniture Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/furnitureshop.WordPress.2022-05-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_furnitureshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/furniture-shop/',
					),


					array(
						'import_file_name'           => 'Outdoor Gear Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/outdoorgear.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_outdoorgear-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/outdoor-gear/',
					),


					array(
						'import_file_name'           => 'Beard Care Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/beardcare.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_beardcare-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/beard-care/',
					),


					array(
						'import_file_name'           => 'Organic Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/organicshop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_organicshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/organic-shop/',
					),


					array(
						'import_file_name'           => 'Bicycle Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/bicycleshop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_bicycleshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/bicycle-shop/',
					),


					array(
						'import_file_name'           => 'Technology Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/technologyshop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_techshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/tech-shop/',
					),


					array(
						'import_file_name'           => 'Flower Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/flowershop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_flowershop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/flower-shop/',
					),


					array(
						'import_file_name'           => 'Footwear Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/footwear.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_footwaer-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/foot-wear/',
					),


					array(
						'import_file_name'           => 'Home Decoration Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/homedecoration.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_homedecoration-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/home-decoration/',
					),


					array(
						'import_file_name'           => 'GYM Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/gymshop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_gymshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/gym-shop/',
					),


					array(
						'import_file_name'           => 'Perfume Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/perfumeshop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_perfumeshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/perfume-shop/',
					),


					array(
						'import_file_name'           => 'Gift Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/gift-shop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_giftshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/gift-shop/',
					),



					array(
						'import_file_name'           => 'Cake Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/dynoshopcakeshop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_cakeshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/dynoshop-cake-shop/',
					),


					array(
						'import_file_name'           => 'Eyewear Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/eyewear.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_eyewearshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/eye-wear/',
					),


					array(
						'import_file_name'           => 'Video Game Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/videogameshop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_videogameshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => '',
					),


					array(
						'import_file_name'           => 'Beauty Care Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/beardcare.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_beautyshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/beauty-shop/',
					),


					array(
						'import_file_name'           => 'Pet Products Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/petshop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_petshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/pet-shop/',
					),


					array(
						'import_file_name'           => 'Boutique Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/boutiqueshop.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_boutiqueshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/pet-shop/',
					),


					array(
						'import_file_name'           => 'Home Appliance Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2022/05/homeappliance.WordPress.2022-05-28.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_homeappliance-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/pet-shop/',
					),
					
					
				);
			}
			add_filter( 'pt-ocdi/import_files', 'wp_dynoshop_demo_import' );


		
			function wp_dynoshop_demo_import_page_setup( $default_settings ) {
				// $lp_options = get_option( 'dynoshop_options' );
				
				// if (isset($lp_options['white_label_theme_name'])){
				// 	$theme_name = $lp_options['white_label_theme_name'];
				// }
				// else {
				// 	$theme_name = 'Dynoshop';
				// }

				$default_settings['parent_slug'] = 'themes.php';
				$default_settings['page_title']  = esc_html__( 'Demo Import' , 'wp-dynoshop' );
				$default_settings['menu_title']  = esc_html__( 'Import Demos' , 'wp-dynoshop' );
				$default_settings['capability']  = 'import';
				// $default_settings['menu_slug']   = 'pt-one-click-demo-import';
			
				return $default_settings;
			}
			add_filter( 'pt-ocdi/plugin_page_setup', 'wp_dynoshop_demo_import_page_setup' );
		
		
			function wp_dynoshop_demo_import_page_title ($plugin_title ) {
				?>
				<h1 class="ocdi__title  dashicons-before  dashicons-upload"><?php $plugin_title  = esc_html_e( 'Dynoshop Demo Import', 'wp-dynoshop' ); ?></h1>
				<?php
			}
			add_filter( 'pt-ocdi/plugin_page_title', 'wp_dynoshop_demo_import_page_title' );
		
			add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

			add_filter( 'pt-ocdi/import_memory_limit', '256M' );
		
		
		
			
		
			// Demo Import Function End






