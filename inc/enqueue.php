<?php

    	// Enqueue scripts and styles.

		function wp_dynoshop_scripts() {
			$theme = wp_get_theme();
			$scripts_version = $theme->get('Version');
			
			wp_enqueue_style('wp_dynoshop_main_style', get_template_directory_uri() . '/css/theme.min.css', array(), $scripts_version);
			
			}
            
			add_action( 'wp_enqueue_scripts', 'wp_dynoshop_scripts', 100);